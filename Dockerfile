FROM nextcloud:31.0.2

RUN apt update && \
    apt install -y --no-install-recommends python3 python3-distutils ffmpeg libbz2-dev && \
    docker-php-ext-install bz2 && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/
    
RUN sed -i 's@\(www-data:.*:\)/usr/sbin/nologin@\1/bin/bash@' /etc/passwd
